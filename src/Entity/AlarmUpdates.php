<?php

namespace App\Entity;

use App\Repository\AlarmUpdatesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AlarmUpdatesRepository::class)
 * @UniqueEntity("site_ip", message="A site with this IP address already exists.")
 */
class AlarmUpdates
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     */
    private $site_ip;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $site_name;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="string", nullable=false)
     */
    private $power;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="string", nullable=false)
     */
    private $optical;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $updated_at;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="string", nullable=false)
     */
    private $msmla;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_1;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_2;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_3;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_4;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_5;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_6;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_7;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_8;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $dipl_3g;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $dipl_4g;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiteIp(): ?string
    {
        return $this->site_ip;
    }

    public function setSiteIp(string $site_ip): self
    {
        $this->site_ip = $site_ip;

        return $this;
    }

    public function getSiteName(): ?string
    {
        return $this->site_name;
    }

    public function setSiteName(string $site_name): self
    {
        $this->site_name = $site_name;

        return $this;
    }

    public function getPower(): ?string
    {
        return $this->power;
    }

    public function setPower(string $power): self
    {
        $this->power = $power;

        return $this;
    }

    public function getOptical(): ?string
    {
        return $this->optical;
    }

    public function setOptical(string $optical): self
    {
        $this->optical = $optical;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getMsmla(): ?string
    {
        return $this->msmla;
    }

    public function setMsmla(string $msmla): self
    {
        $this->msmla = $msmla;

        return $this;
    }

    public function getRutemp1(): ?int
    {
        return $this->rutemp_1;
    }

    public function setRutemp1(int $rutemp_1): self
    {
        $this->rutemp_1 = $rutemp_1;

        return $this;
    }

    public function getRutemp2(): ?int
    {
        return $this->rutemp_2;
    }

    public function setRutemp2(int $rutemp_2): self
    {
        $this->rutemp_2 = $rutemp_2;

        return $this;
    }

    public function getRutemp3(): ?int
    {
        return $this->rutemp_3;
    }

    public function setRutemp3(int $rutemp_3): self
    {
        $this->rutemp_3 = $rutemp_3;

        return $this;
    }

    public function getRutemp4(): ?int
    {
        return $this->rutemp_4;
    }

    public function setRutemp4(int $rutemp_4): self
    {
        $this->rutemp_4 = $rutemp_4;

        return $this;
    }

    public function getRutemp5(): ?int
    {
        return $this->rutemp_5;
    }

    public function setRutemp5(int $rutemp_5): self
    {
        $this->rutemp_5 = $rutemp_5;

        return $this;
    }

    public function getRutemp6(): ?int
    {
        return $this->rutemp_6;
    }

    public function setRutemp6(int $rutemp_6): self
    {
        $this->rutemp_6 = $rutemp_6;

        return $this;
    }

    public function getRutemp7(): ?int
    {
        return $this->rutemp_7;
    }

    public function setRutemp7(int $rutemp_7): self
    {
        $this->rutemp_7 = $rutemp_7;

        return $this;
    }

    public function getRutemp8(): ?int
    {
        return $this->rutemp_8;
    }

    public function setRutemp8(int $rutemp_8): self
    {
        $this->rutemp_8 = $rutemp_8;

        return $this;
    }

    public function getDipl3g(): ?int
    {
        return $this->dipl_3g;
    }

    public function setDipl3g(int $dipl_3g): self
    {
        $this->dipl_3g = $dipl_3g;

        return $this;
    }

    public function getDipl4g(): ?int
    {
        return $this->dipl_4g;
    }

    public function setDipl4g(int $dipl_4g): self
    {
        $this->dipl_4g = $dipl_4g;

        return $this;
    }

}
