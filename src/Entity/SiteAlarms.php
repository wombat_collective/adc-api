<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SiteAlarms
 *
 * @ORM\Table(name="site_alarms")
 * @ORM\Entity(repositoryClass="App\Repository\SiteAlarmsRepository")
 * @UniqueEntity("siteIp", message="A site with this IP adress already exists.")
 */
class SiteAlarms
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="site_name", type="string", length=255, nullable=false)
     */
    private $siteName;

    /**
     * @var string
     *
     * @ORM\Column(name="site_ip", type="string", length=255, nullable=false, unique=true)
     */
    private $siteIp;

    /**
     * @var int
     * @Assert\Type(type="string", message="The value {{ value }} is not valid.")
     * @ORM\Column(name="power_failure", type="string", nullable=false)
     */
    private $powerFailure;

    /**
     * @var int
     * @Assert\Type(type="string", message="The value {{ value }} is not valid.")
     * @ORM\Column(name="optical_transceiver", type="string", nullable=false)
     */
    private $opticalTransceiver;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="received_at", type="datetime", nullable=false, options={"default"="current_timestamp()"})
     */
    private $receivedAt = 'current_timestamp()';

    /**
     * @Assert\Type(type="string", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="string", nullable=false)
     */
    private $msmla;

    /**
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_1;

    /**
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_2;

    /**
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_3;

    /**
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_4;

    /**
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_5;

    /**
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_6;

    /**
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_7;

    /**
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $rutemp_8;

    /**
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $dipl_3g;

    /**
     * @Assert\Type(type="integer", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="integer", nullable=false)
     */
    private $dipl_4g;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiteName(): ?string
    {
        return $this->siteName;
    }

    public function setSiteName(string $siteName): self
    {
        $this->siteName = $siteName;

        return $this;
    }

    public function getSiteIp(): ?string
    {
        return $this->siteIp;
    }

    public function setSiteIp(string $siteIp): self
    {
        $this->siteIp = $siteIp;

        return $this;
    }

    public function getPowerFailure(): ?string
    {
        return $this->powerFailure;
    }

    public function setPowerFailure(string $powerFailure): self
    {
        $this->powerFailure = $powerFailure;

        return $this;
    }

    public function getOpticalTransceiver(): ?string
    {
        return $this->opticalTransceiver;
    }

    public function setOpticalTransceiver(string $opticalTransceiver): self
    {
        $this->opticalTransceiver = $opticalTransceiver;

        return $this;
    }

    public function getReceivedAt(): ?\DateTimeInterface
    {
        return $this->receivedAt;
    }

    public function setReceivedAt(\DateTimeInterface $receivedAt): self
    {
        $this->receivedAt = $receivedAt;

        return $this;
    }

    public function getMsmla(): ?string
    {
        return $this->msmla;
    }

    public function setMsmla(string $msmla): self
    {
        $this->msmla = $msmla;

        return $this;
    }

    public function getRutemp1(): ?int
    {
        return $this->rutemp_1;
    }

    public function setRutemp1(int $rutemp_1): self
    {
        $this->rutemp_1 = $rutemp_1;

        return $this;
    }

    public function getRutemp2(): ?int
    {
        return $this->rutemp_2;
    }

    public function setRutemp2(int $rutemp_2): self
    {
        $this->rutemp_2 = $rutemp_2;

        return $this;
    }

    public function getRutemp3(): ?int
    {
        return $this->rutemp_3;
    }

    public function setRutemp3(int $rutemp_3): self
    {
        $this->rutemp_3 = $rutemp_3;

        return $this;
    }

    public function getRutemp4(): ?int
    {
        return $this->rutemp_4;
    }

    public function setRutemp4(int $rutemp_4): self
    {
        $this->rutemp_4 = $rutemp_4;

        return $this;
    }

    public function getRutemp5(): ?int
    {
        return $this->rutemp_5;
    }

    public function setRutemp5(int $rutemp_5): self
    {
        $this->rutemp_5 = $rutemp_5;

        return $this;
    }

    public function getRutemp6(): ?int
    {
        return $this->rutemp_6;
    }

    public function setRutemp6(int $rutemp_6): self
    {
        $this->rutemp_6 = $rutemp_6;

        return $this;
    }

    public function getRutemp7(): ?int
    {
        return $this->rutemp_7;
    }

    public function setRutemp7(int $rutemp_7): self
    {
        $this->rutemp_7 = $rutemp_7;

        return $this;
    }

    public function getRutemp8(): ?int
    {
        return $this->rutemp_8;
    }

    public function setRutemp8(int $rutemp_8): self
    {
        $this->rutemp_8 = $rutemp_8;

        return $this;
    }

    public function getDipl3g(): ?int
    {
        return $this->dipl_3g;
    }

    public function setDipl3g(int $dipl_3g): self
    {
        $this->dipl_3g = $dipl_3g;

        return $this;
    }

    public function getDipl4g(): ?int
    {
        return $this->dipl_4g;
    }

    public function setDipl4g(int $dipl_4g): self
    {
        $this->dipl_4g = $dipl_4g;

        return $this;
    }


}
