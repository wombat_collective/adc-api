<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Alarms
 *
 * @ORM\Table(name="alarms")
 * @ORM\Entity(repositoryClass="App\Repository\AlarmsRepository")
 */
class Alarms
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"show_alarms"})
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false, options={"default"="current_timestamp()"})
     * @Groups({"show_alarms"})
     */
    private $createdAt = 'current_timestamp()';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false, options={"default"="'0000-00-00 00:00:00'"})
     * @Groups({"show_alarms"})
     */
    private $updatedAt = '\'0000-00-00 00:00:00\'';

    /**
     * @var int
     * @Assert\NotBlank()
     * @Assert\Type(type="string", message="The value {{ value }} is not valid.")
     * @ORM\Column(name="alarm_status", type="string", nullable=false)
     * @Groups({"show_alarms"})
     */
    private $alarmStatus;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"show_alarms"})
     */
    private $alarmCode;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"show_alarms"})
     */
    private $siteIp;

    /**
     * @Assert\NotBlank()
     * @Assert\Type(type="string", message="The value {{ value }} is not valid.")
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"show_alarms"})
     */
    private $siteName;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getAlarmStatus(): ?string
    {
        return $this->alarmStatus;
    }

    public function setAlarmStatus(string $alarmStatus): self
    {
        $this->alarmStatus = $alarmStatus;

        return $this;
    }

    public function getAlarmCode(): ?string
    {
        return $this->alarmCode;
    }

    public function setAlarmCode(string $alarmCode): self
    {
        $this->alarmCode = $alarmCode;

        return $this;
    }

    public function getSiteIp(): ?string
    {
        return $this->siteIp;
    }

    public function setSiteIp(string $siteIp): self
    {
        $this->siteIp = $siteIp;

        return $this;
    }

    public function getSiteName(): ?string
    {
        return $this->siteName;
    }

    public function setSiteName(string $siteName): self
    {
        $this->siteName = $siteName;

        return $this;
    }


}
