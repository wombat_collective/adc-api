<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * AlarmTypes
 *
 * @ORM\Table(name="alarm_types")
 * @ORM\Entity(repositoryClass="App\Repository\AlarmTypesRepository")
 * @UniqueEntity("alarmCode", message="An alarm with this code already exists.")
 */
class AlarmTypes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="alarm_name", type="string", length=255, nullable=false)
     */
    private $alarmName;

    /**
     * @ORM\Column(name="alarm_code", type="string", length=255, nullable=false, unique=true)
     */
    private $alarmCode;

    public function __construct()
    {
        $this->alarms = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAlarmCode(): ?string
    {
        return $this->alarmCode;
    }

    public function getAlarmName(): ?string
    {
        return $this->alarmName;
    }

    public function setAlarmName(string $alarmName): self
    {
        $this->alarmName = $alarmName;

        return $this;
    }

    public function setAlarmCode(string $alarmCode): self
    {
        $this->alarmCode = $alarmCode;

        return $this;
    }

}
