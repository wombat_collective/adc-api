<?php

namespace App\Repository;

use App\Entity\SiteAlarms;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SiteAlarms|null find($id, $lockMode = null, $lockVersion = null)
 * @method SiteAlarms|null findOneBy(array $criteria, array $orderBy = null)
 * @method SiteAlarms[]    findAll()
 * @method SiteAlarms[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiteAlarmsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SiteAlarms::class);
    }

    // /**
    //  * @return SiteAlarms[] Returns an array of SiteAlarms objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SiteAlarms
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
