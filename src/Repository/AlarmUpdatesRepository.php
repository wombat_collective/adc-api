<?php

namespace App\Repository;

use App\Entity\AlarmUpdates;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AlarmUpdates|null find($id, $lockMode = null, $lockVersion = null)
 * @method AlarmUpdates|null findOneBy(array $criteria, array $orderBy = null)
 * @method AlarmUpdates[]    findAll()
 * @method AlarmUpdates[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlarmUpdatesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AlarmUpdates::class);
    }

    /**
     * Find all alarms on a specific date.
     * @param $value
     * @return int|mixed|string
     */
    public function findAllOnDate(string $value)
    {
        return $this->createQueryBuilder('a')
            ->where('str_to_date(a.created_at, \'%Y-%m-%d\') = :created')
            ->setParameter('created', $value)
            ->orderBy('a.created_at', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * Find all dates from a given date range.
     * @param string $from - First date (earliest)
     * @param string $to - Second date (latest)
     * @return array|int|string
     */
    public function findAllFromDates(string $from, string $to)
    {
        $entityManager = $this->getEntityManager();
        $query = $entityManager->createQuery('select d from App\Entity\AlarmUpdates d where str_to_date(d.updated_at, \'%Y-%m-%d\') >= :from and str_to_date(d.updated_at, \'%Y-%m-%d\') <= :to')
        ->setParameters(array('from' => $from, 'to' => $to));

        return $query->getArrayResult();
    }

    // /**
    //  * @return AlarmUpdates[] Returns an array of AlarmUpdates objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AlarmUpdates
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
