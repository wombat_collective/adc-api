<?php

namespace App\Repository;

use App\Entity\AlarmTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AlarmTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method AlarmTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method AlarmTypes[]    findAll()
 * @method AlarmTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlarmTypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AlarmTypes::class);
    }

    // /**
    //  * @return AlarmTypes[] Returns an array of AlarmTypes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AlarmTypes
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
