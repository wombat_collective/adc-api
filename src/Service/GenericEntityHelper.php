<?php

namespace App\Service;

use App\Repository\SiteAlarmsRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use PhpParser\Node\Expr\Array_;

/**
 * Class GenericEntityHelper - provides useful entity management functionality
 * @package App\Service
 */
class GenericEntityHelper {

    private $site_repository;

    public function __construct(SiteAlarmsRepository $sitesRepository)
    {
        $this->site_repository = $sitesRepository;
    }

    /**
     * Find a site using the provided ip address.
     * @param $ip
     * @return \App\Entity\SiteAlarms[]|bool
     */
    public function findSiteByIp($ip)
    {
        $result = $this->site_repository->findBy(['siteIp' => $ip]);
        return !$result ? false : $result;
    }

    /**
     * Find an item by specifying the search criteria.
     *
     * @param $criteria[]
     * @param ServiceEntityRepository $serviceEntityRepository
     * @return bool|object
     */
    public function findByValue($criteria, ServiceEntityRepository $serviceEntityRepository)
    {
        $result = $serviceEntityRepository->findOneBy($criteria);
        return !$result ? false : $result;
    }

    /**
     * Retrieve an alarm type using the alarm code provided.
     * @param $name - Alarm type code.
     * @param ServiceEntityRepository $serviceEntityRepository
     * @return bool|object
     */
    public function matchNameToCode($name, ServiceEntityRepository $serviceEntityRepository) {
        $names = $serviceEntityRepository->findOneBy(['alarmCode' => $name]);
        if (!$names) {
            return false;
        }
        return $names;
    }
}