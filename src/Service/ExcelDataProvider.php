<?php


namespace App\Service;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Exception;

class ExcelDataProvider
{
    private $excelFile;
    private $inputFileType = 'Xlsx';

    public function __construct($excelDirectory)
    {
        $this->excelFile = $excelDirectory . '/book.xlsx';
    }

    /**
     * Read and Excel file.
     * @return mixed
     * @throws Exception
     */
    public function readExcelFile()
    {
        $reader = IOFactory::createReader($this->inputFileType);
        // How many rows we want to read for each 'chunk'
        $chunkSize = 4;
        // Create a new instance of our Read Filter
        $chunkFilter = new ExcelFileReader();

        // Tell the Reader that we want to use the Read Filter
        $reader->setReadFilter($chunkFilter);

        for ($startRow = 2; $startRow <= 20; $startRow += $chunkSize) {
            /**  Tell the Read Filter which rows we want for this iteration.  **/
            $chunkFilter->setRows($startRow,$chunkSize);
            /** Load only the rows that match our filter. */
            $spreadsheet = $reader->load($this->excelFile);

            return $spreadsheet->getActiveSheet()->getCellByColumnAndRow(2,1)->getValue();
        }

    }

    public function getDataRange($range)
    {
        $reader = IOFactory::createReader($this->inputFileType);
        $spreadsheet = $reader->load($this->excelFile);

        return $spreadsheet->getActiveSheet()->rangeToArray($range, NULL, TRUE, TRUE, FALSE);
    }

}