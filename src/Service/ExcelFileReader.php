<?php


namespace App\Service;

use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;
use Symfony\Component\HttpFoundation\File\File;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ExcelFileReader implements IReadFilter
{
    private $startRow = 0;
    private $endRow = 0;

    /**
     * Only read the heading row and the specified rows.
     *
     * @param string $column
     * @param int $row
     * @param string $worksheetName
     * @return bool|void
     */
    public function readCell($column, $row, $worksheetName = '')
    {
        if (($row == 1) || ($row >= $this->startRow && $row < $this->endRow)) {
            return true;
        }
        return false;

    }

    public function setRows($startRow, $chunkSize)
    {
        $this->startRow = $startRow;
        $this->endRow = $startRow + $chunkSize;
    }

}