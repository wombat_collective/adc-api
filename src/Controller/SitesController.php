<?php

namespace App\Controller;

use App\Entity\Sites;
use App\Form\SitesType;
use App\Repository\SitesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class SitesController - handle CRUD operations for alarm sites.
 * @Route("/sites")
 */
class SitesController extends AbstractController
{
    /**
     * List all available sites.
     * 
     * @Route("/", name="sites_index", methods={"GET"})
     * @param SitesRepository $sitesRepository
     * @return Response
     */
    public function index(SitesRepository $sitesRepository): Response
    {
        return $this->render('sites/index.html.twig', [
            'sites' => $sitesRepository->findAll(),
        ]);
    }

    /**
     * Create a new site.
     * 
     * @Route("/new", name="sites_new", methods={"GET","POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function new(Request $request, ValidatorInterface $validator): Response
    {
        $site = new Sites();
        $form = $this->createForm(SitesType::class, $site);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($site);
            $entityManager->flush();

            $errors = $validator->validate($site);
            if (count($errors) > 0) {
                return $this->render('sites/new.html.twig', [
                    'site' => $site,
                    'form' => $form->createView(),
                ]);
            }
            return $this->redirectToRoute('sites_index');
        }

        return $this->render('sites/new.html.twig', [
            'site' => $site,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show the site details specified by the route parameter.
     * 
     * @Route("/{id}", name="sites_show", methods={"GET"})
     * @param Sites $site
     * @return Response
     */
    public function show(Sites $site): Response
    {
        return $this->render('sites/show.html.twig', [
            'site' => $site,
        ]);
    }

    /**
     * Edit a particular site's details.
     * 
     * @Route("/{id}/edit", name="sites_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Sites $site): Response
    {
        $form = $this->createForm(SitesType::class, $site);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sites_index');
        }

        return $this->render('sites/edit.html.twig', [
            'site' => $site,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Delete a particular site.
     *
     * @Route("/{id}", name="sites_delete", methods={"DELETE"})
     * @param Request $request
     * @param Sites $site
     * @return Response
     */
    public function delete(Request $request, Sites $site): Response
    {
        if ($this->isCsrfTokenValid('delete'.$site->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($site);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sites_index');
    }
}
