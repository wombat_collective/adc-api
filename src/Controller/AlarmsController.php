<?php

namespace App\Controller;

use App\Entity\AlarmUpdates;
use App\Entity\SiteAlarms;
use App\Entity\Alarms;
use App\Entity\Sites;
use App\Repository\AlarmsRepository;
use App\Repository\AlarmTypesRepository;
use App\Repository\AlarmUpdatesRepository;
use App\Repository\SiteAlarmsRepository;
use App\Repository\SitesRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Service\GenericEntityHelper;
use Symfony\Component\Serializer\SerializerInterface;


/**
 * Class AlarmsController - handle alarms CRUD operations.
 * @Route("/alarms")
 * @package App\Controller
 */
class AlarmsController extends AbstractController
{
    private $sites_repository;
    private $alarm_types_repository;
    private $alarms_repository;
    private $site_alarms_repository;
    private $alarm_updates_repository;
    private $generic_entity_helper;
    private $validator_handle;
    private $tz_override;

    public function __construct(SitesRepository $sitesRepository, AlarmTypesRepository $alarmTypesRepository,
        GenericEntityHelper $genericEntityHelper, ValidatorInterface $validator, AlarmsRepository $alarmsRepository, SiteAlarmsRepository $siteAlarmsRepository, AlarmUpdatesRepository $alarmUpdatesRepository)
    {
        $this->sites_repository = $sitesRepository;
        $this->alarm_types_repository = $alarmTypesRepository;
        $this->alarms_repository = $alarmsRepository;
        $this->site_alarms_repository = $siteAlarmsRepository;
        $this->alarm_updates_repository = $alarmUpdatesRepository;
        $this->generic_entity_helper = $genericEntityHelper;
        $this->validator_handle = $validator;
        $this->tz_override = new \DateTimeZone('Africa/Nairobi');
    }

	/**
	 * @Route("/", name="alarms_index")
	 */
	public function alarmsIndex()
	{
		return $this->json(array('test' => 'sample test'));
	}

    /**
     * Fetch all alarms available.
     * 
     * @Route("/fetch/all", name="alarms_list")
     * @param SerializerInterface $serializer
     * @param AlarmTypesRepository $alarmTypesRepository
     * @return JsonResponse
     */
    public function alarmsFetchAll(SerializerInterface $serializer, AlarmTypesRepository $alarmTypesRepository)
    {
        $alarms = $this->getDoctrine()->getRepository(Alarms::class)->findAll();
        $alarmType = $alarmTypesRepository->findAll();

        // Shallow copy
        $alarms_arr = $alarms;
        $alarm_types_arr = $alarmType;
        // Use '&' for reference(to gain access to the value).
        foreach ($alarms_arr as $value) {
            foreach ($alarm_types_arr as $v) {
                if ($v->getAlarmCode() == $value->getAlarmCode()) {
                    $value->setAlarmCode($v->getAlarmName());
                }
            }
            
        }

        if (!$alarms) {
            return $this->json(array('message' => 'So much empty.'), 500);
        }
        return $this->json(array('alarms' => $alarms), 200);
    }

    /**
     * Fetch a single alarm.
     *
     * @Route("/fetch/{id}", name="alarms_show", requirements={"id"="\d+"})
     * @param $id - Alarm id
     * @param AlarmsRepository $alarmsRepository
     * @param SerializerInterface $serializer
     * @param AlarmTypesRepository $alarmTypesRepository
     * @return JsonResponse
     */
    public function fetchSingleAlarm($id, AlarmsRepository $alarmsRepository, SerializerInterface $serializer, AlarmTypesRepository $alarmTypesRepository)
    {
        $alarms = $alarms = $this->getDoctrine()->getRepository(Alarms::class)->find($id);
        $alarmType = $alarmTypesRepository->findAll();
        if (!$alarmType || !$alarms) {
            return $this->json(array('message' => 'An error occurred while trying to find the specified alarms codes.'), 500);
        }
        // Shallow copy
        $alarms_arr = $alarms;
        $alarm_types_arr = $alarmType;
        // Use '&' for reference(to gain access to the value).
        foreach ($alarms_arr as $value) {
            foreach ($alarm_types_arr as $v) {
                if ($v->getAlarmCode() == $value->getAlarmCode()) {
                    $value->setAlarmCode($v->getAlarmName());
                }
            }
            
        }
        
        return $this->json(array('alarm' => $alarms_arr), 200);
    }

    /**
     * Fetch all alarms raised on a specific date.
     * 
     * @Route("/fetch/from/{dt}", name="alarms_on_date", requirements={"dt"="(\d{4}\-\d{2}\-\d{2})"})
     * @param $dt
     * @param AlarmTypesRepository $alarmTypesRepository
     * @return JsonResponse
     */
    public function fetchAlarmsOnDate($dt, AlarmTypesRepository $alarmTypesRepository)
    {
        try {
            $fetch_date = new DateTime($dt);
        } catch (\Exception $e) {
            return $this->json(array('error' => 'Please provide a valid date.'), 500);
        }
        /** @method findAllOnDate() - Custom method that returns an array instead of an Entity object. */
        $alarms = $this->getDoctrine()->getRepository(Alarms::class)->findAllOnDate($dt);
        $alarmType = $alarmTypesRepository->findAll();
        if (!$alarmType || !$alarms) {
            return $this->json(array('message' => 'An error occurred while trying to find the specified alarms codes.'), 500);
        }
        // Shallow copy
        $alarms_arr = $alarms;
        $alarm_types_arr = $alarmType;
        // Use '&' for reference(to gain access to the value).
        foreach ($alarms_arr as &$value) {
            foreach ($alarm_types_arr as $v) {
                if ($v->getAlarmCode() == $value["alarmCode"]) {
                    $value["alarmCode"] = $v->getAlarmName();
                }
            }
        }

        return $this->json(array('message' => $alarms_arr), 200);
    }

    /**
     * Fetch alarms from the specified date range.
     *
     * @Route("/fetch/from/range", name="alarms_from_range", methods={"POST"})
     * @param Request $request
     * @param AlarmsRepository $alarmsRepository
     * @return JsonResponse
     */
    public function fetchAlarmsFromRange(Request $request, AlarmsRepository $alarmsRepository)
    {
        $from = $request->get('from');
        $to = $request->get('to');

        if (!isset($request) || empty($request) || is_null($request)) {
            return $this->json(array('message' => 'Empty values are not supported.'), 400);
        }

        $reg = '/^(\d{4}\-\d{2}\-\d{2})$/m';
        if (!preg_match($reg, $from) || !preg_match($reg, $to)){
            return $this->json(array('message' => 'Invalid input provided.'), 400);
        }

        $range = $alarmsRepository->findAllFromDates($from, $to);

        if (!$range) {
            return $this->json(array('message' => 'An error occurred while trying to find alarms from the specified date range.'), 500);
        }

        return $this->json(array('message' => $range), 200);
    }

    /**
     * Fetch alarms raised from the specified site.
     * 
     * @Route("/fetch/from/ip", name="alarms_from_site", methods="POST")
     * @param SerializerInterface $serializer Default serializer
     * @param Request $request
     * @param AlarmTypesRepository $alarmTypesRepository  Needed to provide alarm codes and their respective names
     * @return JsonResponse
     */
    public function fetchAlarmsFromIp(SerializerInterface $serializer, Request $request, AlarmTypesRepository $alarmTypesRepository)
    {
        $req = $request->get('ip');
        // Validation
        if (!isset($req) || empty($req) || is_null($req)) {
            return $this->json(array('message' => 'Empty values are not accepted.'), 400);
        }

        $regex = '/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/m';

        if (!preg_match($regex, $req)) {
            return $this->json(array('message' => 'Please provide a valid IP address.'), 400);
        }

        $alarms = $this->getDoctrine()->getRepository(Alarms::class)->findBy(['siteIp' => $req]);
        $alarmType = $alarmTypesRepository->findAll();
        if (!$alarms || !$alarmType) {
            return $this->json(array('message' => 'An error occurred while trying to find alarms from the specified site.'), 500);
        }
        // Shallow copy
        $alarms_arr = $alarms;
        $alarm_types_arr = $alarmType;
        // Use '&' for reference(to gain access to the value).
        foreach ($alarms_arr as $value) {
            foreach ($alarm_types_arr as $v) {
                if ($v->getAlarmCode() == $value->getAlarmCode()) {
                    $value->setAlarmCode($v->getAlarmName());
                }
            }
            
        }

        return $this->json(array('alarms' => $alarms_arr));
    }

    /**
     * @Route("/fetch/code", name="alarm_fetch_code")
     * @param AlarmsRepository $alarmsRepository
     * @param Request $request
     * @return JsonResponse
     */

    public function getAlarmsByCode(AlarmsRepository $alarmsRepository, Request $request)
    {
        $alarm_code = $request->get('code');

        if (is_null($alarm_code) || empty($alarm_code))
            return $this->json(array("message" => "Invalid input."), 400);

        $alarms = $alarmsRepository->findBy(['alarmCode' => $alarm_code]);

        if (!$alarms)
            return $this->json(array("message" => "An error occurred."), 500);

        return $this->json(array("message" => $alarms), 200);
    }

    /**
     * Main app entry point. Handles creating & updating all alarms.
     *
     * @Route("/write", name="alarms_write", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param GenericEntityHelper $entityHelper
     * @return JsonResponse
     * @throws \Exception
     */
	
	public function alarmsWrite(Request $request, EntityManagerInterface $entityManager, ValidatorInterface $validator, GenericEntityHelper $entityHelper)
	{
        $site_data = $request->toArray();

        if (empty($site_data) || is_null($site_data)) {
            return $this->json(array('error' => 'Some fields are missing.', 'data' => $request), 400);
        }

		$site_name = $site_data['SiteName'];
		$site_ip = $site_data['SiteIp'];
		$power = $site_data['Power'];
		$optical = $site_data['Optical'];
        $msmla = $site_data['Msmla'];
		$rut_1 = $site_data['RUTemp1'];
		$rut_2 = $site_data['RUTemp2'];
		$rut_3 = $site_data['RUTemp3'];
		$rut_4 = $site_data['RUTemp4'];
		$rut_5 = $site_data['RUTemp5'];
		$rut_6 = $site_data['RUTemp6'];
		$rut_7 = $site_data['RUTemp7'];
		$rut_8 = $site_data['RUTemp8'];
		$dipl_3g = $site_data['DIPL3G'];
		$dipl_4g = $site_data['DIPL4G'];

        $entityManager = $this->getDoctrine()->getManager();
        $siteAlarm = new SiteAlarms();

        /* Check whether a site with the provided IP exists.
         * If it doesn't exist, create it else update relevant fields.
         * */

        $site = $this->generic_entity_helper->findByValue(['siteIp' => $site_ip], $this->sites_repository);
        $site_alarms_avail = $this->generic_entity_helper->findByValue(['siteIp' => $site_ip], $this->site_alarms_repository);

        if (!$site) {
            $result_status = array('message' => 'Updated site alarms database.','status' => 200);
        	// Add new site to the sites database
            $site_update_status = $this->updateSites($site_name, $site_ip);

            // Insert values into the site alarms database
            $created_at = new DateTime('now', $this->tz_override);
            // Alarm not found in site_alarms database, insert it.
            if (!$site_alarms_avail) {
                $siteAlarm->setSiteIp($site_ip);
                $siteAlarm->setSiteName($site_name);
                $siteAlarm->setPowerFailure($power);
                $siteAlarm->setOpticalTransceiver($optical);
                $siteAlarm->setMsmla($msmla);
                $siteAlarm->setDipl3g($dipl_3g);
                $siteAlarm->setDipl4g($dipl_4g);
                $siteAlarm->setRutemp1($rut_1);
                $siteAlarm->setRutemp2($rut_2);
                $siteAlarm->setRutemp3($rut_3);
                $siteAlarm->setRutemp4($rut_4);
                $siteAlarm->setRutemp5($rut_5);
                $siteAlarm->setRutemp6($rut_6);
                $siteAlarm->setRutemp7($rut_7);
                $siteAlarm->setRutemp8($rut_8);
                $siteAlarm->setReceivedAt($created_at);

                $validationErrors = $validator->validate($siteAlarm);

                if (count($validationErrors) > 0) {
                    $result_status= array('message' => array('error' => $validationErrors),'status' => 200);
                }

                $entityManager->persist($siteAlarm);
                $entityManager->flush();

            } else {
                // Alarm found in site_alarms database, update it.
                $site_alarms = array(
                    'power' => $power,
                    'optical' => $optical,
                    'msmla' => $msmla,
                    'ip' => $site->getSiteIp(),
                    'name' => $site->getSiteName(),
                    'dipl3g' => $dipl_3g,
                    'dipl4g' => $dipl_4g,
                    'rutemp1' => $rut_1,
                    'rutemp2' => $rut_2,
                    'rutemp3' => $rut_3,
                    'rutemp4' => $rut_4,
                    'rutemp5' => $rut_5,
                    'rutemp6' => $rut_6,
                    'rutemp7' => $rut_7,
                    'rutemp8' => $rut_8,
                );

                $update_site_alarms = $this->updateSiteAlarms($site_alarms);
                if (!$update_site_alarms) {
                    $result_status['status'] = 500;
                    $result_status['message'] = 'Failed trying to update site alarms.';
                }
            }

            if (!$site_update_status){
                $result_status['message'] = 'A site with this IP address already exists.';
                $result_status['status'] = 500;
            }
            $last_created = new DateTime('now');
            // Create an alarm entry/entries
            $alarm_code_array = array(
                '00000302' => $power,
                '0000020E' => $optical,
                '0000030F' => $msmla,
                '08004001' => $dipl_3g,
                '08007001' => $dipl_4g,
                '08000500' => $rut_1,
                '08000501' => $rut_2,
                '08000502' => $rut_3,
                '08000503' => $rut_4,
                '08000504' => $rut_5,
                '08000505' => $rut_6,
                '08000506' => $rut_7,
                '08000507' => $rut_8,
            );
            foreach ($alarm_code_array as $key => $value) {
            	$updated_at = new DateTime('now', $this->tz_override);
                $temp_status = $this->updateAlarms($value, $key, $last_created, $site_ip, $updated_at);
                if (!$temp_status) {
                    $result_status['status'] = 500;
                    $result_status['message'] = 'Failed trying to update site alarms.';
                }
            }

            $alarm_update_status = $this->pushAlarmUpdates($site_ip, $created_at, $last_created, $alarm_code_array);
            if (!$alarm_update_status) {
                $result_status['status'] = 500;
                $result_status['message'] = 'Failed updating the alarms table.';
            }

            return $this->json(array('message' => $result_status['message']), (int)$result_status['status']);
        } else {
            $result_status = array('message' => 'Updated site alarms database.','status' => 200);
            if (!$site_alarms_avail) {
                // Insert values into the site alarms database
                $created = new DateTime('now', $this->tz_override);
                $siteAlarm->setSiteIp($site_ip);
                $siteAlarm->setSiteName($site_name);
                $siteAlarm->setPowerFailure($power);
                $siteAlarm->setOpticalTransceiver($optical);
                $siteAlarm->setMsmla($msmla);
                $siteAlarm->setDipl3g($dipl_3g);
                $siteAlarm->setDipl4g($dipl_4g);
                $siteAlarm->setRutemp1($rut_1);
                $siteAlarm->setRutemp2($rut_2);
                $siteAlarm->setRutemp3($rut_3);
                $siteAlarm->setRutemp4($rut_4);
                $siteAlarm->setRutemp5($rut_5);
                $siteAlarm->setRutemp6($rut_6);
                $siteAlarm->setRutemp7($rut_7);
                $siteAlarm->setRutemp8($rut_8);
                $siteAlarm->setReceivedAt($created);

                $validationErrors = $validator->validate($siteAlarm);

                if (count($validationErrors) > 0) {
                    $result_status= array('message' => array('error' => $validationErrors),'status' => 200);
                }

                $entityManager->persist($siteAlarm);
                $entityManager->flush();
            }

            // Update site alarms values, delegate to an update endpoint
            $site_alarms = array(
                'power' => $power,
                'optical' => $optical,
                'msmla' => $msmla,
                'ip' => $site->getSiteIp(),
                'name' => $site->getSiteName(),
                'dipl3g' => $dipl_3g,
                'dipl4g' => $dipl_4g,
                'rutemp1' => $rut_1,
                'rutemp2' => $rut_2,
                'rutemp3' => $rut_3,
                'rutemp4' => $rut_4,
                'rutemp5' => $rut_5,
                'rutemp6' => $rut_6,
                'rutemp7' => $rut_7,
                'rutemp8' => $rut_8,
            );

            $site_alarms_obj = $entityManager->getRepository(SiteAlarms::class)->findOneBy(['siteIp' => $site->getSiteIp()]);
            $created_at = $site_alarms_obj->getReceivedAt();

            $update_site_alarms = $this->updateSiteAlarms($site_alarms);
            if (!$update_site_alarms) {
                $result_status['status'] = 500;
                $result_status['message'] = 'Failed trying to update site alarms.';
            }

            $updated_at = new DateTime('now', $this->tz_override);
            // Create an alarm entry/entries
            $alarm_code_array = array(
                '00000302' => $power,
                '0000020E' => $optical,
                '0000030F' => $msmla,
                '08004001' => $dipl_3g,
                '08007001' => $dipl_4g,
                '08000500' => $rut_1,
                '08000501' => $rut_2,
                '08000502' => $rut_3,
                '08000503' => $rut_4,
                '08000504' => $rut_5,
                '08000505' => $rut_6,
                '08000506' => $rut_7,
                '08000507' => $rut_8,
            );
           foreach ($alarm_code_array as $key => $value) {
               $temp_status = $this->updateAlarms($value, $key, $created_at, $site_ip, $updated_at);
               if (!$temp_status) {
               		$result_status['status'] = 500;
               		$result_status['message'] = 'Failed updating the alarms table.';
               		break;
               }
           }
           $alarm_update_status = $this->pushAlarmUpdates($site_ip, $updated_at, $created_at, $alarm_code_array);
           if (!$alarm_update_status) {
               $result_status['status'] = 500;
               $result_status['message'] = 'Failed updating the alarms table.';
           }

           return $this->json(array('message' => $result_status['message']), (int)$result_status['status']);
        }

        
	}

    /**
     * Update the site alarms database.
     *
     * @param $alarms
     * @return bool | JsonResponse
     * @throws \Exception
     */
    public function updateSiteAlarms($alarms)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $site = $entityManager->getRepository(SiteAlarms::class)->findOneBy(['siteIp' => $alarms['ip']]);
        if (!$site){
            return false;
        } else {
            $site->setReceivedAt(new DateTime('now', $this->tz_override));
            $site->setOpticalTransceiver($alarms['optical']);
            $site->setPowerFailure($alarms['power']);
            $site->setMsmla($alarms['msmla']);
            $site->setDipl3g($alarms['dipl3g']);
            $site->setDipl4g($alarms['dipl4g']);
            $site->setRutemp1($alarms['rutemp1']);
            $site->setRutemp2($alarms['rutemp2']);
            $site->setRutemp3($alarms['rutemp3']);
            $site->setRutemp4($alarms['rutemp4']);
            $site->setRutemp5($alarms['rutemp5']);
            $site->setRutemp6($alarms['rutemp6']);
            $site->setRutemp7($alarms['rutemp7']);
            $site->setRutemp8($alarms['rutemp8']);

            $validationErrors = $this->validator_handle->validate($site);

            if (count($validationErrors) > 0) {
                return $this->json(array('message' => array('error' => $validationErrors)), 200);
            }
            $entityManager->flush();

            return true;
        }

    }

    /**
     * Handle writing/updating alarms.
     *
     * @param $status
     * @param $alarmCode
     * @param $created_at
     * @param $site_ip
     * @param $updated_at
     * @return bool|JsonResponse
     */
    public function updateAlarms($status, $alarmCode, $created_at, $site_ip, $updated_at)
    {
        $site = $this->generic_entity_helper->findByValue(['siteIp' => $site_ip], $this->sites_repository);
        $alarm_type = $this->generic_entity_helper->findByValue(['alarmCode' => $alarmCode], $this->alarm_types_repository);

        if (!$site && !$alarm_type) {
            return false;
        }

        // Create a new entry or update if one already exists.
        $alarm = new Alarms();
        $entity_manager = $this->getDoctrine()->getManager();
        $alarm->setAlarmStatus($status);
        $alarm->setCreatedAt($created_at);
        $alarm->setUpdatedAt($updated_at);
        $alarm->setAlarmCode($alarmCode);
        $alarm->setSiteIp($site_ip);
        $alarm->setSiteName($site->getSiteName());

        $errors = $this->validator_handle->validate($alarm);
        if (count($errors) > 0){
            return $this->json(array('message' => array('error' => $errors)), 200);
        }

        $entity_manager->persist($alarm);
        $entity_manager->flush();

        return true;
	}

    /**
     * @param $site_ip
     * @param $updated_at
     * @param $created_at
     * @param $alarm_data
     * @return bool|JsonResponse
     */
    public function pushAlarmUpdates($site_ip, $updated_at, $created_at, $alarm_data)
    {
        $site = $this->generic_entity_helper->findByValue(['siteIp' => $site_ip], $this->sites_repository);

        foreach ($alarm_data as $key => $value) {
            $alarm_type = $this->generic_entity_helper->findByValue(['alarmCode' => $key], $this->alarm_types_repository);
            if (!$alarm_type){
                return false;
            }
        }

        if (!$site) {
            return false;
        }

        $alarm_status = $this->generic_entity_helper->findByValue(['site_ip' => $site_ip], $this->alarm_updates_repository);

        if (!$alarm_status) {
            $entity_manager = $this->getDoctrine()->getManager();
            $alarm_update = new AlarmUpdates();
            $alarm_update->setSiteName($site->getSiteName());
            $alarm_update->setSiteIp($site_ip);
            $alarm_update->setPower($alarm_data["00000302"]);
            $alarm_update->setOptical($alarm_data["0000020E"]);
            $alarm_update->setMsmla($alarm_data['0000030F']);
            $alarm_update->setDipl3g($alarm_data['08004001']);
            $alarm_update->setDipl4g($alarm_data['08007001']);
            $alarm_update->setRutemp1($alarm_data['08000500']);
            $alarm_update->setRutemp2($alarm_data['08000501']);
            $alarm_update->setRutemp3($alarm_data['08000502']);
            $alarm_update->setRutemp4($alarm_data['08000503']);
            $alarm_update->setRutemp5($alarm_data['08000504']);
            $alarm_update->setRutemp6($alarm_data['08000505']);
            $alarm_update->setRutemp7($alarm_data['08000506']);
            $alarm_update->setRutemp8($alarm_data['08000507']);
            $alarm_update->setUpdatedAt($updated_at);
            $alarm_update->setCreatedAt($created_at);

            $errors = $this->validator_handle->validate($alarm_update);
            if (count($errors) > 0){
                return $this->json(array('message' => array('error' => $errors)), 200);
            }

            $entity_manager->persist($alarm_update);
            $entity_manager->flush();
        } else {
            $entity_manager = $this->getDoctrine()->getManager();
            $alarmUpdate = $entity_manager->getRepository(AlarmUpdates::class)->findOneBy(['site_ip' => $site_ip]);
            $alarmUpdate->setPower($alarm_data["00000302"]);
            $alarmUpdate->setOptical($alarm_data["0000020E"]);
            $alarmUpdate->setMsmla($alarm_data['0000030F']);
            $alarmUpdate->setDipl3g($alarm_data['08004001']);
            $alarmUpdate->setDipl4g($alarm_data['08007001']);
            $alarmUpdate->setRutemp1($alarm_data['08000500']);
            $alarmUpdate->setRutemp2($alarm_data['08000501']);
            $alarmUpdate->setRutemp3($alarm_data['08000502']);
            $alarmUpdate->setRutemp4($alarm_data['08000503']);
            $alarmUpdate->setRutemp5($alarm_data['08000504']);
            $alarmUpdate->setRutemp6($alarm_data['08000505']);
            $alarmUpdate->setRutemp7($alarm_data['08000506']);
            $alarmUpdate->setRutemp8($alarm_data['08000507']);
            $alarmUpdate->setUpdatedAt($updated_at);

            $errors = $this->validator_handle->validate($alarmUpdate);
            if (count($errors) > 0){
                return $this->json(array('message' => array('error' => $errors)), 200);
            }

            $entity_manager->flush();
        }
        return true;
	}

    /**
     * Update the sites database.
     * 
     * @param $name
     * @param $ip
     * @return bool
     */
    public function updateSites($name, $ip)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $site = new Sites();
        $site->setSiteName($name);
        $site->setSiteIp($ip);

        $validationErrors = $this->validator_handle->validate($site);
        if (count($validationErrors) > 0) {
            return false;
        }

        $entityManager->persist($site);
        $entityManager->flush();

        return true;
	}
}