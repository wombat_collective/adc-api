<?php

namespace App\Controller;

use App\Repository\AlarmUpdatesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class AlarmUpdateController
 * @Route("/alarm/update")
 * @package App\Controller
 */
class AlarmUpdateController extends AbstractController
{
    /**
     * @Route("/", name="alarm_update", methods={"GET"})
     */
    public function index(): Response
    {
        $resp = array('message' => 'Welcome to the alarm updates index.', 'status' => 200);
        return $this->json(array('message' => $resp['message']), $resp['status']);
    }

    /**
     * @Route("/fetch/all", name="alarm_update_all", methods={"GET", "POST"})
     * @param AlarmUpdatesRepository $alarmUpdatesRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function fetchAllUpdate(AlarmUpdatesRepository $alarmUpdatesRepository, SerializerInterface $serializer)
    {
        $updates = $alarmUpdatesRepository->findAll();

        return $this->json(array('message' => $updates), 200);
    }

    /**
     * @Route("/fetch/{id}", name="alarm_fetch_single", requirements={"id"="\d+"}, methods={"GET", "POST"})
     * @param $id
     * @param AlarmUpdatesRepository $alarmUpdatesRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function fetchSingleUpdate($id, AlarmUpdatesRepository $alarmUpdatesRepository, SerializerInterface $serializer)
    {
        $result = array('message' => '','status' => 200);
        $updates = $alarmUpdatesRepository->findBy(['id' => $id]);
        if (!$updates) {
            $result["message"] = "Couldn't find the specified alarm.";
            $result["status"] = 500;
        }

        return $this->json(array('message' => $updates), $result["status"]);
    }

    /**
     * @Route("/fetch/ip", name="alarm_fetch_ip", methods={"POST"})
     * @param Request $request
     * @param AlarmUpdatesRepository $alarmUpdatesRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function fetchSingleSite(Request $request, AlarmUpdatesRepository $alarmUpdatesRepository, SerializerInterface $serializer)
    {
        $result = array('message' => '','status' => 200);
        $ip = $request->get('ip');

        if (empty($ip) || is_null($ip) || !isset($ip)) {
            $result["message"] = "Empty values are not supported";
            $result["status"] = 400;
            return $this->json(array('message' => $result["message"]), $result["status"]);
        }

        $updates = $alarmUpdatesRepository->findBy(['site_ip' => $ip]);

        if (!$updates) {
            $result["message"] = "Could not find the specified alarm.";
            $result["status"] = 500;
            return $this->json(array('message' => $result["message"]), $result["status"]);
        }

        if (empty($updates)) {
            $result = array('message' => 'So much empty.','status' => 200);
            return $this->json(array('message' => $result["message"]), $result["status"]);
        }

        return $this->json(array('message' => $updates), $result["status"]);
    }

    /**
     * @Route("/fetch/date", name="alarm_fetch_date", methods={"POST"})
     * @param Request $request
     * @param AlarmUpdatesRepository $alarmUpdatesRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function fetchAllOnDate(Request $request, AlarmUpdatesRepository $alarmUpdatesRepository, SerializerInterface $serializer)
    {
        $result = array('message' => '','status' => 200);
        $dt = $request->get('date');

        if (empty($dt) || is_null($dt) || !isset($dt)) {
            $result["message"] = "Empty values are not supported";
            $result["status"] = 400;
            return $this->json(array('message' => $result["message"]), $result["status"]);
        }

        $regx = '/^(\d{4}\-\d{2}\-\d{2})$/m';
        if (!preg_match($regx, $dt)){
            $result["message"] = "Invalid input.";
            $result["status"] = 400;
            return $this->json(array('message' => $result["message"]), $result["status"]);
        }

        $updates = $alarmUpdatesRepository->findAllOnDate($dt);

        if (!$updates) {
            $result["message"] = "Could not find the specified alarm.";
            $result["status"] = 500;
            return $this->json(array('message' => $result["message"]), $result["status"]);
        }

        if (empty($updates)) {
            $result = array('message' => 'So much empty.','status' => 200);
            return $this->json(array('message' => $result["message"]), $result["status"]);
        }

        return $this->json(array('message' => $updates), $result["status"]);
    }

    /**
     * @Route("/fetch/date/range", name="alarm_fetch_date_range", methods={"POST"})
     * @param Request $request
     * @param AlarmUpdatesRepository $alarmUpdatesRepository
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */

    public function fetchFromDates(Request $request, AlarmUpdatesRepository $alarmUpdatesRepository, SerializerInterface $serializer)
    {
        $result = array('message' => '','status' => 200);
        $from = $request->get('from');
        $to = $request->get('to');

        // Validation
        if (empty($request) || is_null($request) || !isset($request)) {
            $result["message"] = "Empty values are not supported";
            $result["status"] = 400;
            return $this->json(array('message' => $result["message"]), $result["status"]);
        }

        $reg = '/^(\d{4}\-\d{2}\-\d{2})$/m';

        if(!preg_match($reg, $from) || !preg_match($reg, $to)) {
            $result["message"] = "Invalid parameters provided.";
            $result["status"] = 400;
            return $this->json(array('message' => $result["message"]), $result["status"]);
        }

        $updates = $alarmUpdatesRepository->findAllFromDates($from, $to);

        if (!$updates) {
            $result["message"] = "Could not find the specified alarm.";
            $result["status"] = 500;
            return $this->json(array('message' => $result["message"]), $result["status"]);
        }

        if (empty($updates)) {
            $result = array('message' => 'So much empty.','status' => 200);
            return $this->json(array('message' => $result["message"]), $result["status"]);
        }

        return $this->json(array('message' => $updates), $result["status"]);
    }
}
