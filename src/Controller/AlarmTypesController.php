<?php

namespace App\Controller;

use App\Entity\AlarmTypes;
use App\Form\AlarmTypesType;
use App\Repository\AlarmTypesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AlarmTypesController - handle CRUD operations for alarm types.
 * @Route("/alarm/types")
 */
class AlarmTypesController extends AbstractController
{
    /**
     * List all available alarm types.
     * 
     * @Route("/", name="alarm_types_index", methods={"GET"})
     * @param AlarmTypesRepository $alarmTypesRepository
     * @return Response
     */
    public function index(AlarmTypesRepository $alarmTypesRepository): Response
    {
        return $this->render('alarm_types/index.html.twig', [
            'alarm_types' => $alarmTypesRepository->findAll(),
        ]);
    }

    /**
     * Create a new alarm type.
     * 
     * @Route("/new", name="alarm_types_new", methods={"GET","POST"})
     * @param Request $request
     * @param ValidatorInterface $validator
     * @return Response
     */
    public function new(Request $request, ValidatorInterface $validator): Response
    {
        $alarmType = new AlarmTypes();
        $form = $this->createForm(AlarmTypesType::class, $alarmType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($alarmType);
            $entityManager->flush();

            $error = $validator->validate($alarmType);
            if (count($error) > 0) {
                return $this->render('alarm_types/new.html.twig', [
                    'alarm_type' => $alarmType,
                    'form' => $form->createView(),
                ]);
            }

            return $this->redirectToRoute('alarm_types_index');
        }

        return $this->render('alarm_types/new.html.twig', [
            'alarm_type' => $alarmType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show the alarm type specified by the route parameter.
     *
     * @Route("/{id}", name="alarm_types_show", methods={"GET"})
     * @param AlarmTypes $alarmType
     * @return Response
     */
    public function show(AlarmTypes $alarmType): Response
    {
        return $this->render('alarm_types/show.html.twig', [
            'alarm_type' => $alarmType,
        ]);
    }

    /**
     * Edit alarm type details.
     *
     * @Route("/{id}/edit", name="alarm_types_edit", methods={"GET","POST"})
     * @param Request $request
     * @param AlarmTypes $alarmType
     * @return Response
     */
    public function edit(Request $request, AlarmTypes $alarmType): Response
    {
        $form = $this->createForm(AlarmTypesType::class, $alarmType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('alarm_types_index');
        }

        return $this->render('alarm_types/edit.html.twig', [
            'alarm_type' => $alarmType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Delete an alarm type entry.
     *
     * @Route("/{id}", name="alarm_types_delete", methods={"DELETE"})
     * @param Request $request
     * @param AlarmTypes $alarmType
     * @return Response
     */
    public function delete(Request $request, AlarmTypes $alarmType): Response
    {
        if ($this->isCsrfTokenValid('delete'.$alarmType->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($alarmType);
            $entityManager->flush();
        }

        return $this->redirectToRoute('alarm_types_index');
    }
}
