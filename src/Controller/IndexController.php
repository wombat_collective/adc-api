<?php


namespace App\Controller;


use App\Service\ExcelDataProvider;
use App\Service\ExcelFileReader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndexController - index page provider.
 * @package App\Controller
 */
class IndexController extends AbstractController
{
    private $excelFileHandler;

    public function __construct(ExcelDataProvider $excelDataProvider)
    {
        $this->excelFileHandler = $excelDataProvider;
    }

    /**
     * Provide an endpoint for the homepage.
     *
     * @Route("/", name="api_index", methods={"GET"})
     */
    public function index(): Response
    {
        $response = array("message" => "Welcome to the API index page.");
        return $this->json($response, 200);
    }

    /**
     * Read Excel file and return the data read.
     * 
     * @Route("/read/", name="api_read", methods={"GET"})
     */

    public function readFile()
    {
        $res = $this->excelFileHandler->getDataRange('A2:G3');
        return $this->json($res, 200);
    }

}