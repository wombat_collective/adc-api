<?php

namespace App\Form;

use App\Entity\AlarmTypes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class AlarmTypesType - Alarm types form class.
 * @package App\Form
 */
class AlarmTypesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('alarmName', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'This field is missing.'])
                ],
                'required' => true,
                'attr' => [
                    'placeholder' => 'Alarm Name'
                ]
            ])
            ->add('alarmCode', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'This field is missing.'])
                ],
                'required' => true,
                'attr' => [
                    'placeholder' => '0000003F'
                ],
                'help' => 'The alarms hexadecimal representation.'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AlarmTypes::class,
        ]);
    }
}
