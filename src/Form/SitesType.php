<?php

namespace App\Form;

use App\Entity\Sites;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Ip;

/**
 * Class SitesType - Sites form class
 * @package App\Form
 */
class SitesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('siteName', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'An empty field is not allowed.'])
                ],
                'label' => 'Site Name',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Site A'
                ]
            ])
            ->add('siteIp', TextType::class, [
                'constraints' => [
                    new NotBlank(['message' => 'An empty field is not allowed.']),
                    new Ip(['message' => 'This is not a valid IP address.'])
                ],
                'label' => 'Site IP',
                'required' => true,
                'attr' => [
                    'placeholder' => '127.0.0.1'
                ],
                'help' => 'A valid IP address.'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sites::class,
        ]);
    }
}
