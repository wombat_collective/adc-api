# ADC web application

## Synopsis

A web application that handles the HTTP request/response cycle, data exchange and exposes
 an API that avails data for presentation.

## Description

A web application built using the [Symfony](https://symfony.com/ "Symfony") `PHP` framework.
A working HTTP server is required to run this application. As an alternative, [Symfony](https://symfony.com/ "Symfony") provides a
 wrapper/mechanism to run a server using `PHP`'s builtin server which can be used for development purposes. Note that the last options requires extra configuration
 and is not recommended for production.

## Requirements

* A working HTTP server installation e.g [Apache](http://apache.org/ "Apache HTTP server"), [Nginx](https://nginx.org/ "NGINX")
* `PHP` version >=7.2.5 _(tested using version 7.4.13)_ The following [PHP](https://www.php.net/ "PHP") extensions should be enabled:
    * `Ctype`
    * `iconv`
    * `JSON`
    * `PCRE`
    * `Session`
    * `Simple XML`
    * `Tokenizer`
    > Any additional extensions used will be subsequently added to this list.
 * [Composer](https://getcomposer.org/ "Composer") - Package manager for `PHP` based projects/applications.
 * [Yarn](https://yarnpkg.com/ "Yarn") - package manager for Javascript and other web resources.
 * A database backend e.g `MySQL`/`Postgres`/`MariaDB`. This project is configured to work with `MariDB 10.5.8` out of the box.
    > Database related configuration values must be configured on project installation.

## Installation/Setup

Typical `PHP`/`Composer` installation should work fine.

```sh
# Clone the repository to your server web root.
git clone https://japodhidev@bitbucket.org/wombat_collective/adc-api.git

cd adc-api/

# Install dependencies.
composer install

# Deployment setup.
composer dump-env prod

# Install/Update your vendors.
composer install --no-dev --optimize-autoloader

# Clear you Symfony cache.
APP_ENV=prod APP_DEBUG=0 php bin/console cache:clear
```

Additional information regarding deploying `Symfony` applications can be found [here](https://symfony.com/doc/current/deployment.html "Deployment").

`Apache` (Virtual hosts) configuration sample:

```apacheconfig
<VirtualHost *:80>
    # Server admin email.
    ServerAdmin japodhidev@gmail.com
    # Server domain name (should match the entry in your hosts file).
    ServerName adc.test
    ServerAlias www.adc.test
    # DocumentRoot should be accessible to the current user.
    DocumentRoot "/srv/http/adc-api/public"
    DirectoryIndex /index.php
    # Log directories may vary depending on the hosting OS environment. 
    ErrorLog "/var/log/httpd/adc.test-error_log"
    CustomLog "/var/log/httpd/adc.test-access_log" combined
	
    <Directory "/srv/http/adc-api/public">
        AllowOverride None
        Order Allow,Deny
        Allow from All

        FallBackResource /index.php
    </Directory>
</VirtualHost>
```

#### Yarn dependencies

Run the following commands to set up web assets:

```sh
# Install yarn dependencies
yarn install

# Build Encore/Webpack assets
yarn encore production
``` 

#### Doctrine setup

The project requires that the `DATABASE_URL` environment variable to point to a valid database DSN. The _.env_/_.env.local.php_ contain the necessary variable definitions.
